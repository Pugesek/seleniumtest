package com.cg.spring;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class BrowseAmazonTest {
	public static void TestAmazon() {
		WebDriver driver = new ChromeDriver();

		driver.get("https://www.amazon.com/");

		WebElement searchBar = driver.findElement(By.id("twotabsearchtextbox"));
		searchBar.sendKeys("dr. catsby");
		searchBar.submit();
		
		WebElement firstPick = driver.findElement(By.xpath("//*[@id=\"search\"]/div[1]/div[2]/div/span[4]/div[1]/div[2]/div/span/div/div/div/div/div[2]/div[2]/div/div[1]/div/div/div[1]/h2/a/span"));
		firstPick.click();
		
		WebElement addToCart = driver.findElement(By.id("add-to-cart-button"));
		addToCart.click();
		
		WebElement goToCart = driver.findElement(By.id("hlb-ptc-btn-native"));
		goToCart.click();
		
		
		System.out.println("Finished Amazon Test");
		
		
	}
}
