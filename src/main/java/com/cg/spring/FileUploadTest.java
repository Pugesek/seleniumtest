package com.cg.spring;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class FileUploadTest {
	public static void TestUpload() {
		/*
		 * This test was done using a custom spring boot web application
		 * 
		 * */
		WebDriver driver = new ChromeDriver();

		driver.get("http://localhost:xxxx/");

		WebElement uploadButton = driver.findElement(By.id("uploadthis"));
		WebElement submitButton = driver.findElement(By.id("submitthis"));

		uploadButton.sendKeys("C:\\Users\\username\\Documents\\someExcelName.xlsx");
		submitButton.click();
	}
}
